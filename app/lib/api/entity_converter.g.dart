// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'entity_converter.dart';

// **************************************************************************
// NpLogGenerator
// **************************************************************************

extension _$ApiFileConverterNpLog on ApiFileConverter {
  // ignore: unused_element
  Logger get _log => log;

  static final log = Logger("api.entity_converter.ApiFileConverter");
}
