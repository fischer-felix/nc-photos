// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data_source.dart';

// **************************************************************************
// NpLogGenerator
// **************************************************************************

extension _$FaceRemoteDataSourceNpLog on FaceRemoteDataSource {
  // ignore: unused_element
  Logger get _log => log;

  static final log = Logger("entity.face.data_source.FaceRemoteDataSource");
}
