// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api.dart';

// **************************************************************************
// NpLogGenerator
// **************************************************************************

extension _$ApiNpLog on Api {
  // ignore: unused_element
  Logger get _log => log;

  static final log = Logger("src.api.Api");
}

extension _$ApiOcsDavDirectNpLog on ApiOcsDavDirect {
  // ignore: unused_element
  Logger get _log => log;

  static final log = Logger("src.api.ApiOcsDavDirect");
}

extension _$ApiOcsFacerecognitionPersonsNpLog on ApiOcsFacerecognitionPersons {
  // ignore: unused_element
  Logger get _log => log;

  static final log = Logger("src.api.ApiOcsFacerecognitionPersons");
}

extension _$ApiOcsFacerecognitionPersonFacesNpLog
    on ApiOcsFacerecognitionPersonFaces {
  // ignore: unused_element
  Logger get _log => log;

  static final log = Logger("src.api.ApiOcsFacerecognitionPersonFaces");
}

extension _$ApiFilesNpLog on ApiFiles {
  // ignore: unused_element
  Logger get _log => log;

  static final log = Logger("src.api.ApiFiles");
}

extension _$ApiOcsFilesSharingSharesNpLog on ApiOcsFilesSharingShares {
  // ignore: unused_element
  Logger get _log => log;

  static final log = Logger("src.api.ApiOcsFilesSharingShares");
}

extension _$ApiOcsFilesSharingShareNpLog on ApiOcsFilesSharingShare {
  // ignore: unused_element
  Logger get _log => log;

  static final log = Logger("src.api.ApiOcsFilesSharingShare");
}

extension _$ApiOcsFilesSharingShareesNpLog on ApiOcsFilesSharingSharees {
  // ignore: unused_element
  Logger get _log => log;

  static final log = Logger("src.api.ApiOcsFilesSharingSharees");
}

extension _$ApiSystemtagsNpLog on ApiSystemtags {
  // ignore: unused_element
  Logger get _log => log;

  static final log = Logger("src.api.ApiSystemtags");
}

extension _$ApiSystemtagsRelationsFilesNpLog on ApiSystemtagsRelationsFiles {
  // ignore: unused_element
  Logger get _log => log;

  static final log = Logger("src.api.ApiSystemtagsRelationsFiles");
}
