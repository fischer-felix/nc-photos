// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'person_parser.dart';

// **************************************************************************
// NpLogGenerator
// **************************************************************************

extension _$PersonParserNpLog on PersonParser {
  // ignore: unused_element
  Logger get _log => log;

  static final log = Logger("src.entity.person_parser.PersonParser");
}
