bool isHttpStatusGood(int status) => status ~/ 100 == 2;
